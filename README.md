# Starship Dotfiles
My personal Starship Prompt configuration.

# Installation
1. Git clone the repository to: `~/.config/starship` (`git clone https://gitlab.com/Oglo12/starship-config.git ~/.config/starship`)
2. Set the environment variable `STARSHIP_CONFIG` to: `$HOME/.config/starship/starship.toml`
3. Run the init command for your specific shell. (Bash: `eval "$(starship init bash)"`)
4. (BASH) Add this line to your .bashrc file: `source ~/.config/starship/lib/bash.sh`

# Upgrading Config
1. Run the command `upstar`. If that doesn't work, run this: `~/.config/starship/update.sh`
